export let spinnerService = {
  batLoading: () => {
    document.getElementById("loading").style.display = " flex";
    // khi bật loading thì ẩn bảng
    document.getElementById("content").style.display = " none";
  },
  tatLoading: () => {
    document.getElementById("loading").style.display = "none";
    // khi tắt loading thì hiện bảng
    document.getElementById("content").style.display = "block";
  },
};
